import numpy as np
import copy as cp
import matplotlib.pyplot as plt
import random

class Point:
	def __init__(self, x, y, label):
		self.x = x
		self.y = y
		self.predict_label = 0
		self.temp_label = 0
		self.label = label # -1 means center

class Kmeans:
	
	def squareDistance(self,p1, p2):
		return (p1.x-p2.x)**2 + (p1.y-p2.y)**2

	
	def setAssociation(self):
		for i in range(len(self.data)):
			for j in range(i+1,len(self.data)):
				if data[i].predict_label == data[j].predict_label:
					self.cluster_association[data[j].predict_label] += self.kernel[i][j]
		for i in range(self.cluster_num):
			self.cluster_association[i] = self.cluster_association[i]/float(self.cluster_size[i]**2)
		return

	def kernelSpaceDistance(self, cluster, data_index):
		temp = 0
		for i in range(len(self.data)):
			if self.data[i].predict_label == cluster:
				temp += self.kernel[data_index][i]
		temp = -1*(temp*2 / float(self.cluster_size[cluster]))
		return temp + self.cluster_association[cluster]

	def genKernel(self):
		kernel = np.zeros((len(self.data),len(self.data)))
		#use RBF kernel
		sigma = 4.0
		for i in range(len(self.data)):
			for j in range(len(self.data)):
				if i!=j:
					base = 2*(sigma**2)
					kernel[i][j] = kernel[j][i] = float(-self.squareDistance(self.data[i],self.data[j])/base)
		kernel = np.exp(kernel)
		return kernel

	def initial_step(self):
		for i in range(len(self.data)):
			cluster = 0
			min_dis = self.squareDistance(self.data[i],self.centers[0])
			for j in range(1,self.cluster_num):
				if self.squareDistance(self.data[i],self.centers[j])<min_dis:
					cluster = j
					min_dis = self.squareDistance(self.data[i],self.centers[j])
			self.data[i].predict_label = cluster
			self.cluster_size[cluster] += 1
		self.setAssociation()

	def E_step(self):
		coverage = True
		kernel = self.genKernel()
		for i in range(len(self.data)):
			cluster = 0
			min_dis = self.kernelSpaceDistance(0,i)
			for j in range(1,self.cluster_num):
				if self.kernelSpaceDistance(j,i) < min_dis:
					cluster = j
					min_dis = self.kernelSpaceDistance(j,i)
			self.data[i].temp_label = cluster
		for i in range(len(self.data)):
			#update labels
			if self.data[i].predict_label != self.data[i].temp_label:
				coverage = False
			self.cluster_size[self.data[i].predict_label] -= 1
			self.cluster_size[self.data[i].temp_label] += 1
			self.data[i].predict_label = self.data[i].temp_label
		#reset association
		self.setAssociation()
		return coverage
	
	def M_step(self):
		# update cluster center
		new_x = [0] * self.cluster_num
		new_y = [0] * self.cluster_num
		cluster_count = [0] * self.cluster_num
		for i in range(len(self.data)):
			cluster = self.data[i].predict_label
			new_x[cluster] += self.data[i].x
			new_y[cluster] += self.data[i].y
			cluster_count[cluster] += 1
		for i in range(self.cluster_num):
			#update and see if coverage
			if cluster_count[i]>0:
				new_x[i] = new_x[i]/cluster_count[i]
				new_y[i] = new_y[i]/cluster_count[i]
			else :
				new_x[i] = self.centers[i].x
				new_y[i] = self.centers[i].y
			self.centers[i].x = new_x[i]
			self.centers[i].y = new_y[i]
		return 

	def drawResult(self,iteration):
		plt.clf()
		#for animation
		labelColorMap = { 0:'skyblue', 1:'pink', 2:'crimson', 3:'m', 4:'y', 5:'c', 6:'palegreen' }
		labels = []
		x = []
		y = []
		for i in range(len(self.data)):
			cluster = self.data[i].predict_label
			labels.append(labelColorMap[cluster])
			x.append(self.data[i].x)
			y.append(self.data[i].y)
		plt.scatter(x,y,c=labels)
		name = "img/kernelkmeans"+str(iteration)+'.png'
		plt.savefig(name)
		

	def KmeansAlgorithm(self):
		self.drawResult(0)
		self.initial_step()
		coverage = False
		iteration = 1
		while not coverage:
			coverage = self.E_step()
			self.drawResult(iteration)
			iteration += 1
		print 'total interation: ',iteration



	def genCenters(self):
		centers = []
		for i in range(self.cluster_num):
			k = random.randint(0,len(self.data))
			centers.append(self.data[k])
		return centers

	def __init__(self, data, cluster_num):
		self.data = data
		self.cluster_num = cluster_num
		self.cluster_size = [0]*cluster_num
		self.cluster_association = [0]*cluster_num
		self.kernel = self.genKernel()
		self.centers = self.genCenters()
		self.KmeansAlgorithm()
		plt.show()

		
def readInput(case_data,case_label):
	data = []
	f = open(case_data,'r')
	f1 = open(case_label,'r')
	while True:
		line = f.readline()
		label = f1.readline()
		if line == '' or label == '':
			break
		p = line.split()
		#print p," ",label
		temp = Point(float(p[0]),float(p[1]),int(label))
		data.append(temp)
	f.close()
	f1.close()
	return data

if __name__ == '__main__':
	case_data = "test2_data.txt"
	case_label = "test2_ground.txt"
	data = readInput(case_data,case_label)
	cluster_num = int(raw_input('>>> enter cluster number :'))
	#cluster_num = 2
	Kmeans(data,cluster_num)
