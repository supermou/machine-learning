import numpy as np
import copy as cp
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
from numpy import linalg as LA
import random

class hyperPoint:
	def __init__(self,coordinates):
		self.coordinates = coordinates
		self.predict_label = 0

class Point:
	def __init__(self, x, y, label):
		self.x = x
		self.y = y
		self.predict_label = 0
		self.temp_label = 0
		self.label = label # -1 means center

class SpectralClustering:
	
	def squareDistance(self,p1, p2):
		return (p1.x-p2.x)**2 + (p1.y-p2.y)**2

	def squareDistanceHyper(self,p1,p2):
		result = 0
		for i in range(len(p1.coordinates)):
			result += (p1.coordinates[i]-p2.coordinates[i])**2
		return result

	
	def genKernel(self):
		kernel = np.zeros((len(self.data),len(self.data)))
		#use RBF kernel
		for i in range(len(self.data)):
			for j in range(len(self.data)):
				if i!=j:
					base = 2*(self.sigma**2)
					kernel[i][j] = kernel[j][i] = float(-self.squareDistance(self.data[i],self.data[j])/base)
		kernel = np.exp(kernel)
		return kernel

	def genLaplacian(self):
		D = np.identity(self.W.shape[0])
		D_2 = np.identity(self.W.shape[0])
		for i in range(self.W.shape[0]):
			temp = 0
			for j in range(self.W.shape[1]):
				temp += self.W[i][j]
			D_2[i][i] = temp**(-0.5)
			D[i][i] = temp
		L = D - self.W
		result = D_2.dot(L.dot(D_2))
		#print W
		#print 'D: ',D
		#print 'L: ',L[0:3,0:3]
		return result

	def solveEigen(self):
		#eigenvalues are returning in ascending order
		#only cares the first k values and corresponding vector
		L = self.genLaplacian()
		allvalues, allvectors = LA.eigh(L)
		values = allvalues[0:cluster_num]
		vectors = allvectors[:,0:cluster_num]
		#print round((L.dot(allvectors[:,0])-values[0]*vectors[:,0])[5],15)
		hyperdata = []
		for i in range(len(vectors)):
			temp = []
			base = 0
			for j in range(len(vectors[0])):
				temp.append(vectors[i][j])
				base += vectors[i][j]**2
			base = base**0.5
			for j in range(len(vectors[0])):
				temp[j] = temp[j]/base
			hyperdata.append(hyperPoint(temp))
		return hyperdata
	
	def E_step(self):
		coverage = True
		kernel = self.genKernel()
		for i in range(len(self.hyper_data)):
			cluster = 0
			min_dis = self.squareDistanceHyper(self.centers[0],self.hyper_data[i])
			for j in range(1,self.cluster_num):
				if self.squareDistanceHyper(self.centers[j],self.hyper_data[i]) < min_dis:
					cluster = j
					min_dis = self.squareDistanceHyper(self.centers[j],self.hyper_data[i])
			self.data[i].temp_label = cluster
		for i in range(len(self.data)):
			#update labels
			if self.data[i].predict_label != self.data[i].temp_label:
				coverage = False
			self.data[i].predict_label = self.data[i].temp_label
		return coverage
	
	def M_step(self):
		# update cluster center(hyper dimension)
		new_center = []
		for i in range(self.cluster_num):
			temp = [0] * self.cluster_num
			new_center.append(temp)
		cluster_count = [0] * self.cluster_num
		for i in range(len(self.data)):
			cluster = self.data[i].predict_label
			for j in range(self.cluster_num):
				new_center[cluster][j] += self.hyper_data[i].coordinates[j]
			cluster_count[cluster] += 1
		for i in range(self.cluster_num):
			if cluster_count[i]>0:
				for j in range(self.cluster_num):
					self.centers[i].coordinates[j] = new_center[i][j]/cluster_count[i]
		return

	def drawHyperData(self):
		plt.clf() #clear figure
		#draw data points
		labelColorMap = { 0:'skyblue', 1:'pink', 2:'crimson', 3:'m', 4:'y', 5:'c', 6:'palegreen' }
		labels = []
		x = []
		y = []
		for i in range(len(self.hyper_data)):
			cluster = self.data[i].predict_label
			labels.append(labelColorMap[cluster])
			x.append(self.hyper_data[i].coordinates[0])
			y.append(self.hyper_data[i].coordinates[1])
		plt.scatter(x,y,c=labels)

	def drawResult(self,iteration):
		plt.clf() #clear figure
		#draw data points
		labelColorMap = { 0:'skyblue', 1:'pink', 2:'crimson', 3:'m', 4:'y', 5:'c', 6:'palegreen' }
		labels = []
		x = []
		y = []
		for i in range(len(self.data)):
			cluster = self.data[i].predict_label
			labels.append(labelColorMap[cluster])
			x.append(self.data[i].x)
			y.append(self.data[i].y)
		plt.scatter(x,y,c=labels)

		fname = "img/spectral"+str(iteration)+'.png'
		plt.savefig(fname)
		#plt.show()

	def runAlgorithm(self):
		self.drawResult(0)
		coverage = False
		iteration = 1
		while not coverage:
			#print "iteration ",iteration
			coverage = self.E_step()
			self.M_step()
			self.drawResult(iteration)
			iteration += 1
		print 'total interation: ',iteration-1


	def genHyperCenters(self):
		centers = []
		for i in range(self.cluster_num):
			k = random.randint(0,len(self.hyper_data))
			centers.append(self.hyper_data[k])
		return centers

	def __init__(self, data, cluster_num):
		self.data = data
		self.cluster_num = cluster_num
		self.sigma = 4
		self.W = self.genKernel() # here kerel = our weight
		self.hyper_data = self.solveEigen()
		self.centers = self.genHyperCenters()
		self.runAlgorithm()
		#self.drawHyperData()
		plt.show()


		
def readInput(case_data,case_label):
	data = []
	f = open(case_data,'r')
	f1 = open(case_label,'r')
	while True:
		line = f.readline()
		label = f1.readline()
		if line == '' or label == '':
			break
		p = line.split()
		#print p," ",label
		temp = Point(float(p[0]),float(p[1]),int(label))
		data.append(temp)
	f.close()
	f1.close()
	return data

if __name__ == '__main__':
	case_data = "test2_data.txt"
	case_label = "test2_ground.txt"
	data = readInput(case_data,case_label)
	cluster_num = int(raw_input('>>> enter cluster number :'))
	#cluster_num = 2
	SpectralClustering(data,cluster_num)