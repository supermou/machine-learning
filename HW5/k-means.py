import numpy as np
import copy as cp
import matplotlib.pyplot as plt
import random

class Point:
	def __init__(self, x, y, label):
		self.x = x
		self.y = y
		self.predict_label = 0
		self.label = label # -1 means center

class Kmeans:
	
	def distance(self,p1, p2):
		return (p1.x-p2.x)**2 + (p1.y-p2.y)**2

	def E_step(self):
		coverage = True
		for i in range(len(self.data)):
			cluster = 0
			min_dis = self.distance(self.data[i],self.centers[0])
			for j in range(1,self.cluster_num):
				if self.distance(self.data[i],self.centers[j])<min_dis:
					cluster = j
					min_dis = self.distance(self.data[i],self.centers[j])
			if self.data[i].predict_label != cluster:
				coverage = False
			self.data[i].predict_label = cluster
		return coverage
	
	def M_step(self):
		# update cluster center
		new_x = [0] * self.cluster_num
		new_y = [0] * self.cluster_num
		cluster_count = [0] * self.cluster_num
		for i in range(len(self.data)):
			cluster = self.data[i].predict_label
			new_x[cluster] += self.data[i].x
			new_y[cluster] += self.data[i].y
			cluster_count[cluster] += 1
		
		for i in range(self.cluster_num):
			#update and see if coverage
			if cluster_count[i]>0:
				new_x[i] = new_x[i]/cluster_count[i]
				new_y[i] = new_y[i]/cluster_count[i]
			else :
				new_x[i] = self.centers[i].x
				new_y[i] = self.centers[i].y
			self.centers[i].x = new_x[i]
			self.centers[i].y = new_y[i]
		return  

	def drawResult(self,iteration):
		plt.clf()
		labelColorMap = { 0:'skyblue', 1:'pink', 2:'c', 3:'m', 4:'y', 5:'crimson', 6:'palegreen' }
		labels = []
		x = []
		y = []
		for i in range(len(self.data)):
			cluster = self.data[i].predict_label
			labels.append(labelColorMap[cluster])
			x.append(self.data[i].x)
			y.append(self.data[i].y)
		plt.scatter(x,y,c=labels)

		#draw center
		cx = []
		cy = []
		for i in range(len(self.centers)):
			cx.append(self.centers[i].x)
			cy.append(self.centers[i].y)
			labels.append(labelColorMap[i])
		plt.scatter(cx,cy,c='r',marker ="*",s=120)
		name = "img/kmeans"+str(iteration)+'.png'
		plt.savefig(name)

	def KmeansAlgorithm(self):
		self.drawResult(0)
		coverage = False
		iteration = 1
		while not coverage:
			coverage = self.E_step()
			self.M_step()
			self.drawResult(iteration)
			iteration += 1
		print 'total interation: ',iteration-1

	def genCentersOptimized(self):
		centers = []
		k = random.randint(0,len(self.data))
		centers.append(self.data[k])
		max_ = []
		max_.append(k)
		for j in range(0,self.cluster_num-1):
			max_dis = 0
			max_index = max_[-1]
			for i in range(0,len(self.data)):
				d = 0
				for q in range(0,len(max_)):
					d += self.distance(self.data[q],self.data[i])
				if d > max_dis:
					exist = False
					for p in range(len(max_)):
						if max_[p] == i:
							exist = True
					if not exist:	
						max_dis = d
						max_index = i
			max_.append(max_index)
			print max_dis
			centers.append(self.data[max_index])
		return centers
	
	def genCenters(self):
		centers = []
		for i in range(self.cluster_num):
			k = random.randint(0,len(self.data))
			centers.append(self.data[k])
			#centers.append(Point(0,0,-1))
		return centers

	def __init__(self, data, cluster_num):
		self.data = data
		self.cluster_num = cluster_num
		self.centers = self.genCenters()
		self.KmeansAlgorithm()
		plt.show()


		
def readInput(case_data,case_label):
	data = []
	f = open(case_data,'r')
	f1 = open(case_label,'r')
	while True:
		line = f.readline()
		label = f1.readline()
		if line == '' or label == '':
			break
		p = line.split()
		#print p," ",label
		temp = Point(float(p[0]),float(p[1]),int(label))
		data.append(temp)
	f.close()
	f1.close()
	return data

if __name__ == '__main__':
	case_data = "test1_data.txt"
	case_label = "test1_ground.txt"
	data = readInput(case_data,case_label)
	cluster_num = int(raw_input('>>> enter cluster number :'))
	#cluster_num = 2
	Kmeans(data,cluster_num)
