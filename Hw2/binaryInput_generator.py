import random
import numpy as np

def generator_fix():
	
	#the number of lines, initially set 100
	line_num = 100
	line_num = int(raw_input('>>> enter total line number of binary input:'))
	ratio = float(raw_input('>>> enter ratio of 1:'))
	while ratio>1 or ratio<0:
		print "ratio should be in the range [0,1]"
		ratio = float(raw_input('>>> enter ratio of 1:'))

	
	print "#----------------------------------"
	print "generating online learning input..."
	
	#the file that save the binary strings
	f = open('binaryInput.txt', 'w')
	
	for i in range(line_num):		
		count=[0]*2
		line = ""
		binaryCount = random.randint(10,50)
		for j in range(binaryCount):
			temp = random.randint(0,1)
			if count[1] >= binaryCount*ratio :
				line += '0'
			elif count[0] >= binaryCount*(1-ratio) :
				line += '1'
			else :
				count[temp]+=1
				line += str(temp)
		f.write(line+'\n')
	f.close()

	print "generating procedure done with ",line_num," lines of binary string generated"
	print "#----------------------------------"
	return

def generator():
	
	#the number of lines, initially set 100
	line_num = 100
	line_num = int(raw_input('>>> enter total line number of binary input:'))
	
	print "#----------------------------------"
	print "generating online learning input..."
	
	#the file that save the binary strings
	f = open('binaryInput.txt', 'w')
	
	for i in range(line_num):		
		line = ""
		binaryCount = random.randint(10,50)
		for j in range(binaryCount):
			line += str(random.randint(0,1))
		f.write(line+'\n')
	f.close()

	print "generating procedure done with ",line_num," lines of binary string generated"
	print "#----------------------------------"
	return

def version():
	ver = int(raw_input('>>> enter 0 for random version, 1 for fix probability version:'))
	if ver == 1:
		generator_fix()
	elif ver == 0:
		generator()
		

#if the program is running
if __name__ == '__main__':
	version()
