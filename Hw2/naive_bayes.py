
# coding: utf-8

from mnist import MNIST
import numpy as np
import matplotlib.pyplot as plt
import random
import copy as cp
import math
import scipy.stats


class Dataset:
    def __init__(self,images,labels):
        self.images = images;
        self.labels = labels;
    def normalize_image(self):
        for i in range(len(self.images[0])):
            for j in range(len(self.images[1])):
                self.images[i][j] = self.images[i][j]/255.0
        return

class gaussianNaiveBayes:
    def __init__(self,digit,train_set,label_count):
        self.train_data = train_set
        self.digit = digit
        self.label_count = label_count
        self.prior = 0.0
        self.N = len(train_set.labels)
        self.n_features = len(train_set.images[0])
        self.mean = np.zeros(len(train_set.images[0]), dtype=np.float)
        self.var = np.zeros(len(train_set.images[0]), dtype=np.float)

    def setParams(self):
        self.prior = self.label_count/float(self.N)
        
        #set mean
        for i in range(len(self.train_data.images[0])):
            if self.train_data.labels[i] == self.digit:
                for j in range(self.n_features):
                    self.mean[j] += self.train_data.images[i][j]
        self.mean = self.mean / self.label_count

        #set variance
        for i in range(len(self.train_data.images[0])):
            if self.train_data.labels[i] == self.digit:
                for j in range(self.n_features):
                    self.var[j] += ((self.train_data.images[i][j]-self.mean[j])**2)
        self.var = self.var / self.label_count 


    def log_likelihood(self, test_image):
        log_prior = -np.log(self.prior)
        x = cp.copy(test_image)
        for i in range(len(x)):
            x[i] = self.gaussian_wrap(x[i],self.mean[i],self.var[i])
        #printlist(x)
        #sum of each feature(product of each feature in original scale)
        log_posterior = -np.sum(x) 
        return log_prior + log_posterior

    def gaussion_distribution(self,x,mean,var):
        if var ==0 or mean == 0:
            return 0.0
        temp = 1/math.sqrt(2 * math.pi * var)
        temp2 = -1/float(2 * var)*((x-mean)** 2)
        temp2 = temp*np.exp(temp2)
        return temp2
    def gaussian_wrap(self, x, mean, var):
        epsiron = 1.0e-5
        if var < epsiron:
            return 0.0;
        return scipy.stats.norm(mean, var).logpdf(x)

def printlist(x):
    line = ''
    for i in x:
        line +=str(round(i,3))
        line +='  '
    print line 
    return

def plotMnist(x):
    x_ = np.array(x)
    x__ = np.reshape(x_,(28,28))
    plt.matshow(x__, cmap = plt.get_cmap('gray'))
    plt.show()
    return

def getLabelCount(label):
    label_count = [0]*10
    for i in range(len(label)):
        label_count[label[i]]+=1
    return label_count

def create_Models(train):
    resultModels = []
    label_count = getLabelCount(train.labels)
    for i in range(10):
        temp_m = gaussianNaiveBayes(i,train,label_count[i])
        temp_m.setParams()
        resultModels.append(temp_m)
    return resultModels

def testification(models,test_image,answer):
    check = 0
    max_term = 0
    max_label = 0
    for i in range(10):
        temp = models[i].log_likelihood(test_image)
        #print 'posterior of class ',i,' : ',models[i].posterior
        if(temp>max_term):
            max_term = temp
            max_label = i
    if max_label == answer:
        check = 1
    print 'predict:',max_label,', answer: ',answer
    return check


def main(train,test):    
    #create models and set priors for them
    digitModels = create_Models(train)
    print 'models successfully created'
    
    accuracy = 0
    for i in range(100):
        #print 'processing picture ',i,' ...'
        accuracy += testification(digitModels,test.images[i],test.labels[i])
    accuracy = accuracy/float(len(test.labels))
    print 'accuracy = ',accuracy
    #plotMnist(digitModels[7].gaussian)

    return

def readMnist():    
    #load MNIST data
    print "loading mnist data..."
    mndata = MNIST('./data')
    train_images, train_labels = mndata.load_training()
    test_images, test_labels = mndata.load_testing()

    print "complete loading. ",
    print "image num:",len(train_images),", label num:",len(train_labels)

    #prepare dataset for main program
    train = Dataset(train_images,train_labels)
    train.normalize_image()
    test = Dataset(test_images,test_labels)
    test.normalize_image()
    
    #start main program
    main(train,test)
    return


#if the program is running
if __name__ == '__main__':
    readMnist()