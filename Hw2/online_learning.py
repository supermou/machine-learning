
# coding: utf-8

from mnist import MNIST
import numpy as np
import matplotlib.pyplot as plt
import random
import copy as cp
from scipy.stats import beta as Beta
import matplotlib.pyplot as plt
import pylab


class binomialTerm:
    def __init__(self,binaryString):
        self.N = len(binaryString)-1
        self.m = 0
        self.p = 0
        self.s = binaryString
    def setParam(self):
        result = 0
        for x in self.s:
            if x !='\n' and x == '1':
                result += 1
        self.m = result
        self.p = self.m/float(self.N)

def factorial(x):
    product = 1
    for i in range(2,x+1):
        product *= i
    return product 
    
def gammaFunction(x):
    product = factorial(x-1)
    return product

def betaFunction(p,a,b):
    temp = gammaFunction(a+b)
    temp = float(temp*(p**(a-1)))
    result = temp*((1-p)**(b-1))/(gammaFunction(a)*gammaFunction(b))
    return result

def drawBeta(a,b,name):
    x = np.linspace(0.0,1.0,num=100, endpoint = True)
    y = [betaFunction(i,a,b) for i in x]
    plt.plot(x, y,label=name)#original
    
    return

def readInput():    
    #load MNIST data
    f = open('binaryInput.txt','r')
    likelihood = []
    line_num = 0
    while True:
        line = f.readline()
        if line == '':
            break
        line_num += 1
        temp = binomialTerm(line)
        temp.setParam()
        likelihood.append(temp)
    return likelihood,line_num

def main():
    binomials, test_times = readInput()
    alpha = int(raw_input('>>> enter initial alpha: '))
    beta = int(raw_input('>>> enter initial beta: '))
    print "########################################"
    print "#     result of online learning        #"
    print "########################################"
    for i in range(test_times):
        likelihood = binomials[i].p
        #prior = betaFunction(binomials[i].p, alpha, beta)
        #prior = Beta.pdf(binomials[i].p, alpha, beta)
        
        #update alpha and beta for next iteration
        alpha += binomials[i].m
        beta = binomials[i].N - binomials[i].m + beta
        posterior = Beta.pdf(binomials[i].p, alpha, beta)
        posterior2 = betaFunction(binomials[i].p, alpha, beta)       

        #print out answer for this iteration
        print "* iteration ",i
        print 'new alpha = ',alpha
        print 'new beta = ',beta
        print "binomial likelihood =",likelihood
        label = "MLE="+str(likelihood)
        print 'test: ',posterior,':',posterior2
        drawBeta(alpha,beta,label)
        #print "prior*likelihood= ",posterior2
        print "----------------------------------------"
    
    #set plot properties
    pylab.legend(loc='upper left')
    plt.xlabel("p")
    plt.ylabel("beta")
    plt.show()
#if the program is running
if __name__ == '__main__':
    main()