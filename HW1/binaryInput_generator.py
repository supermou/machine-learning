import random
import numpy as np

def generator():
	
	#the number of lines, initially set 100
	line_num = 100
	line_num = int(raw_input('>>> enter total line number of binary input:'))
	
	print "generating online learning input..."
	
	#the file that save the binary strings
	f = open('binaryInput.txt', 'w')
	
	for i in range(line_num):		
		line = ""
		binaryCount = randint(0,line_num)
		for j in range(binaryCount):
			line += random.randint(0,1)
		f.write(line+'\n')
	f.close()

	print "generating procedure done with "line_num," lines of binary string generated"
	return

#if the program is running
if __name__ == '__main__':
	generator()
