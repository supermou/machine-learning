"""
machine learning homework#1
student id : 0656062
"""

import numpy as np
import copy as cp
import matplotlib.pyplot as plt
import sys

class DataPoint:
	def __init__(self, x, y):
		self.x = x
		self.y = y

def Transpose(X):
	X2 = np.ones((X.shape[1],X.shape[0]))
	
	for i in range(X.shape[0]):
		for j in range(X.shape[1]):
			X2[j][i] = X[i][j]

	return X2 

def MatrixMul( x1, x2):
	x1size = x1.shape
	x2size = x2.shape
	
	#new a result matrix
	x = np.ones((x1size[0],x2size[1]))
	#check if the matrices can be multiplied
	if x1size[1]!=x2size[0]:
		ErrorMsg(2)
	#do the multiplication
	for i in range(x1size[0]):#iterates x1's row
		for j in range(x2size[1]):#iterates x2's column
			mul_sum = 0
			for p in range(x1size[1]):#multiply each element
				mul_sum += x1[i][p]*x2[p][j]
			x[i][j] = mul_sum	
	""" 
	print 'multiplication verify'
	x3 = x1.dot(x2)
	for i in range(x1size[0]):
		for j in range(x2size[1]):
			if x[i][j]!=x3[i][j]:
				print str(i)+':'+str(j)+'------>'+str(x[i][j])
				print str(i)+':'+str(j)+'------>'+str(x3[i][j])
	print '--------'
	"""
	return x



def calculateLSE(x1,x2):	
	error = 0
	for i in range(x1.shape[0]):
		error += (x2[i][0] - x1[i][0])*(x2[i][0] - x1[i][0])
	return error 

def drawPolynomial(X, Y1, Y2, xRange):
	plt.plot(X, Y1, "o")#original
	plt.plot(X, Y2, "ro")#calculate result
	plt.xlabel("x")
	plt.ylabel("y")
	plt.xlim(-xRange,xRange)
	plt.show()
	return

def ErrorMsg(msg):
	if msg == 1:
		print "can't find inverse!"
	if msg == 2:
		print "the two matrices can't be multiplied"
	sys.exit

def printMatrix(X):
	size = X.shape
	rows = size[0]
	cols = size[1]
	for i in range(rows):
		string ='[ '
		for j in range(cols):
			string = string + str(round(X[i][j],10))+' '
		print string+']'
	return

def GaussJordanElimination(X):
	#print '---------------------'
	X_b = cp.copy(X)
	size = X.shape
	rows = size[0]
	cols = size[1]
	I = np.identity(rows)
	#print 'first in gauss'
	#printMatrix(X)
	#printMatrix(I)
	#modify the matrix to implement gaussJordan more easier
	for i in range(rows):
		if X[i][i] == 0:
			p = i+1
			find = False
			while p < rows and find == False:
				if(X[p][i]!=0):
					#swap rows
					for q in range(cols):							
						swap = X[p][q]
						X[p][q] = X[i][q]
						X[i][q] = swap
						swap = I[p][q]
						I[p][q] = I[i][q]
						I[i][q] = swap
					find = True
			if find == False:
				ErrorMsg(1)
				return
		if X[i][i]!= 1:
			delta = X[i][i]
			#set to 1
			for p in range(cols):
				X[i][p] = X[i][p]/delta
				I[i][p] = I[i][p]/delta
		for q in range(i+1, rows):
			delta = X[q][i]/X[i][i]
			for j in range(cols):
				X[q][j] = X[q][j] - delta*X[i][j]
				I[q][j] = I[q][j] - delta*I[i][j]
			#printMatrix(X)
	#now we are having a upper triangle
	#print 'upper triangle:'
	#printMatrix(X)
	#printMatrix(I)
	i = rows - 1
	j = cols - 1
	while i >= 0 and j>=0:
		for p in range(i):
			delta = X[p][j] / X[i][j]
			for q in range(cols):
				X[p][q] = X[p][q] - delta * X[i][q]
				I[p][q] = I[p][q] - delta * I[i][q]
		i = i - 1
		j = j - 1
	#print 'elimination:'
	#printMatrix(X)
	#printMatrix(I)

	#print 'verify inverse we get:'
	#printMatrix(X_b.dot(I))
	#print '---------------------'
	return I

			


def constructMatrices(datapoints, degree,lambda_):
	
	#construct matrix A & A^T
	A = np.ones((len(datapoints),degree+1))
	#fill last column with 1s
	row_index = 0
	column_index = (degree+1) - 1
	for i in range(len(datapoints)):
		A[i][column_index] = 1
	column_index = column_index - 1
	#fill the x squares
	while row_index < len(datapoints):
		while column_index >= 0:
			A[row_index][column_index] = A[row_index][column_index+1] * datapoints[row_index].x
			column_index = column_index - 1 
		column_index = (degree+1) - 2
		row_index = row_index + 1
	A_b = cp.copy(A)
	A_T = Transpose(A)

	#construct degree+1*degree+1 size I to get lambda*I
	lambda_I = lambda_ * np.identity(degree+1)
	#print 'lambda'
	#printMatrix(lambda_I)
	
	#result = A^TA + lambda_I 
	result = MatrixMul(A_T,A) + lambda_I
	#find result's inverse by Gauss-Jordan method
	result = GaussJordanElimination(result)

	#multiply b
	b = np.ones((len(datapoints),1))
	for i in range(0,len(datapoints)):
		b[i][0] = datapoints[i].y
	result = MatrixMul(result,A_T)
	#printMatrix(result)
	X = MatrixMul(result,b)
	Y = MatrixMul(A_b,X)
	MatrixMul(A_b,X)
	#printMatrix(b)
	#printMatrix(Y)
	return b,Y,X

def setParameters():
	#f_path = raw_input('>>> enter file path:')
	f = open('datapoints.txt','r')
	datapoints = []
	X = []
	while True:
		line = f.readline()
		point = []
		if line == '':
			break
		point = line.split(',',2)
		datapoints.append(DataPoint(float(point[0]),float(point[1])))
		X.append(float(point[0]))

	base_num = int(raw_input('>>> enter base number:'))
	#base_num = 5
	lambda_ = int(raw_input('>>> enter lambda: '))
	#lambda_ = 2
	
	#b = original y, y = the calculated y
	b,y,C = constructMatrices(datapoints, base_num, lambda_)
	
	print 'coefficent matrix: '
	printMatrix(C)
	print 'error square sum:'
	print calculateLSE(b,y)
	
	#prepare for drawing
	Y1 = []
	Y2 = []
	Y1 = np.reshape( b, len(datapoints)).tolist()
	Y2 = np.reshape( y, len(datapoints)).tolist()
	
	#Draw 
	print 'showing final result...'
	drawPolynomial(X, Y1, Y2, 3.14)
	return

#if the program is running
if __name__ == '__main__':
	setParameters()






