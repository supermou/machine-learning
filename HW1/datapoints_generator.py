import random
import numpy as np

def generator():
	
	#the number of datapoints
	points_num = 200

	#the file that save the points
	f = open('datapoints.txt', 'w')
	
	for i in range(points_num):		
		#generate random x
		x = random.uniform(-3.14,3.14)
		#set the y-x function
		y = np.sin(x) + np.cos(x)
		f.write(str(x)+','+str(y)+'\n')
	f.close()
	return

#if the program is running
if __name__ == '__main__':
	generator()
