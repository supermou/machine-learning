import numpy as np

def printMatrix(X):
	size = X.shape
	rows = size[0]
	
	cols = size[1]
	for i in range(rows):
		string ='[ '
		for j in range(cols):
			string = string + str(round(X[i][j],10))+' '
		print string+']'
	return

def Transpose(X):
	X2 = np.ones((X.shape[1],X.shape[0]))
	
	for i in range(X.shape[0]):
		for j in range(X.shape[1]):
			X2[j][i] = X[i][j]

	return X2 

def MatrixMul( x1, x2):
	x1size = x1.shape
	x2size = x2.shape
	if len(x1size)==1:
		x1size.append(1)
	if len(x2size)==1:
		x2size.append(1)
	#new a result matrix
	x = np.ones((x1size[0],x2size[1]))
	#check if the matrices can be multiplied
	if x1size[1]!=x2size[0]:
		ErrorMsg(2)
	#do the multiplication
	for i in range(x1size[0]):#iterates x1's row
		for j in range(x2size[1]):#iterates x2's column
			mul_sum = 0
			for p in range(x1size[1]):#multiply each element
				mul_sum += x1[i][p]*x2[p][j]
			x[i][j] = mul_sum
	
	return x

def main():
	
	x1 = np.array([[1,2,3],[4,5,6]])
	x2 = np.array([[1],[2],[7]])
	x = MatrixMul(x1,x2)
	
	print Transpose(x1)

	return

#if the program is running
if __name__ == '__main__':
	main()