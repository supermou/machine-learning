import numpy as np
import math
import random
import matplotlib.pyplot as plt


"""
Gaussian generator class version 
"""
class genGaussian:		
	def __drawPDF(self,x):
		interval = self.sigma/float(10)
		X = []
		Y = []
		a = -4*self.sigma
		b = a +interval
		#count = 0
		while b<4*self.sigma:
			#print a,',',b
			X.append((a+b)/float(2))
			y = 0
			for i in x:
				if (i-self.mean)<b and (i-self.mean)>a:
					y += 1
			Y.append(y)
			a = a +interval
			b = b + interval
			#count += 1
		plt.plot(X, Y,"o")#original
		plt.show()
		#plt.xlabel("x")
		#plt.ylabel("y")
		return

	def __dumpTxt(self,filename):
		f = open(filename, 'w')
		f.write(self.txtContext)
		f.close()
		return

	def __generator(self):
		#Y = []		
		for i in range(0,self.sample_num):
			u = random.uniform(0,1)
			v = random.uniform(0,1)
			#print 'in generator'
			#print 'u=',u,' v=',v,' mean=',self.mean
			x = math.sqrt((-2) * math.log(u)) * math.cos(2 * math.pi *v) * self.sigma + self.mean
			#print 'x= ',x
			#y = math.sqrt((-2) * math.log(u)) * math.sin(2 * math.pi *v) * sigma + mean
			self.data.append(x)
			self.txtContext = self.txtContext + str(x) +'\n'
			#Y.append(y)
		return

	def __init__(self,mean, var, num):
		self.mean =  mean
		self.sigma = math.sqrt(var)
		self.txtContext =""
		self.sample_num = num
		self.data = []
		self.__generator()
		return

	

	
