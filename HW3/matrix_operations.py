import numpy as np
import copy as cp

def Transpose(X):
	X2 = np.ones((X.shape[1],X.shape[0]))
	
	for i in range(X.shape[0]):
		for j in range(X.shape[1]):
			X2[j][i] = X[i][j]

	return X2 

def MatrixMul(x1, x2):
	x1size = x1.shape
	x2size = x2.shape
	
	#new a result matrix
	x = np.ones((x1size[0],x2size[1]))
	#check if the matrices can be multiplied
	if x1size[1]!=x2size[0]:
		ErrorMsg(2)
	#do the multiplication
	for i in range(x1size[0]):#iterates x1's row
		for j in range(x2size[1]):#iterates x2's column
			mul_sum = 0
			for p in range(x1size[1]):#multiply each element
				mul_sum += x1[i][p]*x2[p][j]
			x[i][j] = mul_sum		
	return x

def GaussJordanElimination(X_b):
	X = cp.copy(X_b)
	size = X.shape
	rows = size[0]
	cols = size[1]
	I = np.identity(rows)
	
	for i in range(rows):
		if X[i][i] == 0:
			p = i+1
			find = False
			while p < rows and find == False:
				if(X[p][i]!=0):
					#swap rows
					for q in range(cols):							
						swap = X[p][q]
						X[p][q] = X[i][q]
						X[i][q] = swap
						swap = I[p][q]
						I[p][q] = I[i][q]
						I[i][q] = swap
					find = True
			if find == False:
				ErrorMsg(1)
				return
		if X[i][i]!= 1:
			delta = X[i][i]
			#set to 1
			for p in range(cols):
				X[i][p] = X[i][p]/delta
				I[i][p] = I[i][p]/delta
		for q in range(i+1, rows):
			delta = X[q][i]/X[i][i]
			for j in range(cols):
				X[q][j] = X[q][j] - delta*X[i][j]
				I[q][j] = I[q][j] - delta*I[i][j]

	i = rows - 1
	j = cols - 1
	while i >= 0 and j>=0:
		for p in range(i):
			delta = X[p][j] / X[i][j]
			for q in range(cols):
				X[p][q] = X[p][q] - delta * X[i][q]
				I[p][q] = I[p][q] - delta * I[i][q]
		i = i - 1
		j = j - 1
	
	return I