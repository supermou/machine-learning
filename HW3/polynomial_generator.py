import numpy as np
import math
import random
import matplotlib.pyplot as plt
from genGaussian import genGaussian
import matrix_operations as matOP

"""
input :
	a , basis
output :
	y =phi(x)+ b~N(0,a) 
	* note that y is array type
"""

def genCoefficient(basis):
	W = []
	for i in range(basis):
		c = random.uniform(-10,10)
		W.append(c)
	W_ = np.array(W).reshape(len(W),1)
	#print W_.shape
	return W_

def genX(basis,num):
	X1 = []
	for j in range(num):
		temp = []
		temp.append(1)
		x = random.uniform(-10,10)
		print 'x:\n',x
		for i in range(basis-1):
			temp.append(x)
			x = x*x
		X1.append(temp)
	X_ = np.array(X1)
	#print X_.shape
	return X_

def main():
	basis = int(raw_input('>>> enter basis number: '))
	a = int(raw_input('>>> enter a : '))
	sampleNum = int(raw_input('>>> enter sample number: '))
	W = genCoefficient(basis)
	print 'W:\n',W
	#gen 100 data
	#sampleNum = 20 
	bias = np.array(genGaussian(0,a,sampleNum).data).reshape(sampleNum,1)
	x = genX(basis,sampleNum)
	#print bias.shape
	Y = matOP.MatrixMul(x,W)+bias
	print 'y:\n',Y
	#print Y.shape
	return


#if the program is running
if __name__ == '__main__':
	main()