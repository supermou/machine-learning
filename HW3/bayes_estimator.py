import numpy as np
import math
import random
import matplotlib.pyplot as plt
from genGaussian import genGaussian
from genPoly import genPolynomialData
import matrix_operations as matOP
import copy as cp

"""
bayes linear regression estimator
"""


def genW(basis):
	W = []
	for i in range(basis):
		c = random.randint(-10,10)
		W.append(c)
	W_ = np.array(W).reshape(len(W),1)
	#print W_.shape
	return W_

def genMultiGaussian(mean,var):
	W = np.random.multivariate_normal(mean,var)
	return W

def PredictedDistribution(Mn_1D,Sn,phi_x,beta):
	Mn = Mn_1D.reshape(Mn_1D.shape[0],1)
	Mn_t = matOP.Transpose(Mn)
	mean = matOP.MatrixMul(Mn_t,phi_x)
	phi_x_t = matOP.Transpose(phi_x)
	var = 1/float(beta)+phi_x_t.dot(Sn.dot(phi_x))
	return mean,var

def genPhi(x_array,W):
	phi = np.array(x_array)	
	return phi 

def updateParameters(M0,S0,y,phi,beta):
#Sn
	S0_inv = matOP.GaussJordanElimination(S0)
	#phi_t = np.transpose(phi)
	phi_t = matOP.Transpose(phi)

	Sn_inv = S0_inv + beta * matOP.MatrixMul(phi_t,phi)
	Sn =  matOP.GaussJordanElimination(Sn_inv)
	
#Mn
	Mn = Sn.dot(S0_inv.dot(M0).reshape(len(M0),1)+beta*(phi_t.dot(y)))
	Mn_ = Mn.reshape(Mn.shape[0])

	return Mn_,Sn

def bayesEstimator():
#read input parameter to gen data	
	print '#### generate input data ####'
	basis = int(raw_input('>>> enter basis number for W: '))
	a = float(raw_input('>>> enter a for e~N(0,a): '))
	#basis = 2
	#a = 0.04 #noise variance
	real_W = genW(basis)
	
	print "\n#### start online learning ####"

#initialize W_learn by given mean = 0, var = alpha*I
	mean = np.zeros(basis)
	covar = np.identity(basis) * a
	M0 = cp.copy(mean)
	S0 = cp.copy(covar)
	beta = 1/a
	print 'beta:\n',beta
	x_array = []
	y = []
	y_learn = []
	error = []
	W = genMultiGaussian(mean,covar)
#start learning
	count = 0

	while count<100:
		temp = genPolynomialData(basis,real_W,a)
		x_array.append(temp.X)
		y.append(temp.Y)
		W = genMultiGaussian(mean,covar)
		phi = genPhi(x_array,W)
		y_a = np.array(y).reshape(len(y),1)
		y_learn.append(W.dot(temp.X))
		mean,covar = updateParameters(M0,S0,y_a,phi,beta)
		p_mean,p_covar = PredictedDistribution(mean,covar,np.array(temp.X).reshape(basis,1),beta)
		count += 1
		
		
		err = real_W.reshape(basis)-mean
		#print [abs(i) for i in err]
		error.append([abs(i) for i in err])
		#error.append(mean)
		
		#print W
		#if count == 100:
		print "#-----------"
		print 'observe point: (',temp.X[1],temp.Y,')'
		print 'mean:\n',mean
		print 'covar:\n',covar
		print 'predictive distribution mean:\n',p_mean[0][0]
		print 'predictive distribution variance:\n',p_covar[0][0]
		print "#-----------"
	print 'initial W: \n',real_W.reshape(basis)
	err = np.array(y)-np.array(y_learn)
	temp = np.arange(0,count)
	for j in range(basis):
		plt.plot(temp,[i[j] for i in error],label='W'+str(j)+' mean error')
	#plt.plot(temp,[abs(i) for i in err],label='err')
	#plt.plot(temp,y,label='t')
	#plt.plot(temp,y_learn,label='y')
	
	plt.legend()
	plt.show()
	
	return

	


#if the program is running
if __name__ == '__main__':
	bayesEstimator()