# title:
    2017.10.29 machine learning hw3 
#project description:
    this assignment implement sequential estimator and bayes linear regression
#file description:
     1. bayes estimator: 
        the implementation of bayes linear regression.
     2. gaussian generator:
        generates data by guassion distribution given mean and variance
        (could set sample number)
     3. genGaussion: 
        class version of gaussian generator
     4. polynomial generator:
        generate y = Wx+b where b~N(0,a)
        output points number can be set
        *input parameters: basis, a
        *note that W is random matrix
     5. genPoly:
        class version of polynomial generator but W is given as input parameter. Output only on point
        self.X = [1 x^2 x^3...], to get x value please use self.X[2]
     6. sequential estimator:
        implementation of online learning
        input a x from N~(mean,variance) in each iteration, train out the mean and variance from the points 
