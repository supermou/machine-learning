import numpy as np
import math
import random
import matplotlib.pyplot as plt

"""
purpose:
	generate univariate gaussian data
input:
	mean, variance
	* Note that data amount is set 10000 by default
	  and default interval value is set to be sigma/10
output:
	univariate gaussian data
	* drawPDF function shows the pdf of data, but it is close by default

"""

def drawPDF(x,sigma,mean):
	interval = sigma/float(10)
	X = []
	Y = []
	a = -4*sigma
	b = a +interval
	#count = 0
	while b<4*sigma:
		#print a,',',b
		X.append((a+b)/float(2))
		y = 0
		for i in x:
			if (i-mean)<b and (i-mean)>a:
				y += 1
		Y.append(y)
		a = a +interval
		b = b + interval
		#count += 1
	plt.plot(X, Y,"o")#original
	plt.show()
	#plt.xlabel("x")
	#plt.ylabel("y")
	return


def generator(mean,sigma,sample_num):
	X = []
	#Y = []
	f = open('uniGaussianInput.txt', 'w')
	line =""
	#sample_num = 10000
	for i in range(0,sample_num):
		u = random.uniform(0,1)
		v = random.uniform(0,1)
		x = math.sqrt((-2) * math.log(u)) * math.cos(2 * math.pi *v) * sigma + mean
		#y = math.sqrt((-2) * math.log(u)) * math.sin(2 * math.pi *v) * sigma + mean
		X.append(x)
		line = line + str(x) +'\n'
		#Y.append(y)
	drawPDF(X,sigma,mean)
	f.write(line+'\n')
	f.close()
	return

def main():
	mean = int(raw_input('>>> enter mean: '))
	sigma = math.sqrt(int(raw_input('>>> enter variance: ')))
	sample_num = int(raw_input('>>> enter sample number: '))
	generator(mean,sigma,sample_num)
	return


#if the program is running
if __name__ == '__main__':
	main()