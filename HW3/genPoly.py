import numpy as np
import math
import random
import matplotlib.pyplot as plt
from genGaussian import genGaussian
import matrix_operations as matOP

"""
input :
	basis, sampleNum, W , bias(b)
output :
	y =phi(x)+ b~N(0,a) 
#only gen one point at a time
"""

class genPolynomialData:
	def __genX(self,basis):
		temp = []
		temp.append(1)
		x = random.uniform(-10,10)
		for i in range(basis-1):
			temp.append(x)
			x = x*x
		self.X = temp
		X_ = np.array(temp).reshape(1,basis)
		#print 'X shape: ',X_.shape
		return X_

	def __generator(self):
		x = self.__genX(self.basis)
		#print 'gen x: ',x
		self.Y = matOP.MatrixMul(x,self.W)[0][0]+self.bias
		return

	def __init__(self,basis,W,a):
		self.basis = basis
		self.Y = 0
		self.X = []
		self.W = W
		self.bias = genGaussian(0,a,1).data[0]
		self.__generator()
		return

