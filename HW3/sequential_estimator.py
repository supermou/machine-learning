import numpy as np
import math
import random
import matplotlib.pyplot as plt
from genGaussian import genGaussian
import matrix_operations as matOP

"""
sequential estimator
"""
def calVar(old_var,delta,n):
	var = (n-2)/float(n-1)*old_var
	var += (delta**2)/float(n)
	return var

def sequentialEstimator():
	mean = int(raw_input('>>> enter mean: '))
	var = int(raw_input('>>> enter variance: '))	
	#sigma = math.sqrt(var)
#do initialization
	sampleNum = 1
	initial_data = genGaussian(mean,var,sampleNum).data[0]
	#print 'initial_data = ',initial_data 
	n = 1
	new_mean = initial_data
	new_var = 0
	count = 0
	coverage = 0
#start learning	
	mean_error = []
	var_error = []
	while True :
		#print 'var:\n',new_var
		#if coverage>100:		
		if count>1000:
			break
		n += 1
		new_data = genGaussian(mean,var,sampleNum).data[0]
		delta = new_data - new_mean
		new_mean += delta/float(n)
		new_var = calVar(new_var,delta,n)
		mean_error.append(abs(new_mean-mean))
		var_error.append(abs(new_var-var))
		count += 1
#print result for every iteration
		print '#------iteration ',count
		print 'new data = ',new_data
		print 'mean = ',new_mean
		print 'var = ', new_var
		print '#------------------'
		if abs(new_mean-mean)<0.01 :
			coverage += 1
		elif coverage>0:
			coverage -= 1
		"""
		if count ==
		9999:
			print '#iteration ',count
			print 'new data = ',new_data
			print 'mean = ',new_mean
			print 'var = ', new_var
		"""
	#print 'spend ',count,' iterations to achieve coverage'	
#draw learning situation
	temp = np.arange(0,count)
	print 'original mean: ',mean
	print 'original variance: ',var	
	plt.plot(temp,mean_error,label='mean error')
	plt.plot(temp,var_error,label='variance error')
	plt.legend()
	plt.show()
	return


#if the program is running
if __name__ == '__main__':
	sequentialEstimator()